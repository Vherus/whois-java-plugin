/**
 * 
 */
package com.vherus.whois;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import com.servoy.j2db.scripting.IScriptObject;

/**
 * @author Nathan King <nkvherus@gmail.com>
 *
 */
@SuppressWarnings("deprecation")
public class WhoisPluginProvider implements IScriptObject {
	
	private String server = "whois.iana.org";
	private int port = 43;
	private int timeout = 30 * 1000;
	
	@Override
	public Class<?>[] getAllReturnedTypes() {
		
		return null;
		
	}

	@Override
	public String[] getParameterNames(String arg0) {
		
		return new String[] {"domainName", "[server]", "[port]", "[timeout] "};
		
	}

	@Override
	public String getSample(String methodName) {
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("\t// You call the whois server by providing a domain name, getting the info in return: \n");
		buffer.append("\t// var result = plugins.whois.query('stratcorrman.com');  \n");
		buffer.append("\t// You can provide an alternate server (default is iana.org): \n");
		buffer.append("\t// var result = plugins.whois.query('stratcorrman.com','whois.internic.net'); \n");
		buffer.append("\t// You can also provide a port if not standard (43 by default): \n");
		buffer.append("\t// var result = plugins.whois.query('stratcorrman.com','whois.internic.net',43); \n");
		buffer.append("\t// You can also provide a timeout length (in ms, default is 30 seconds): \n");
		buffer.append("\t// var result = plugins.whois.query('stratcorrman.com','whois.internic.net',43,60000); \n");
		
		return buffer.toString();
		
	}

	@Override
	public String getToolTip(String arg0) {
		
		return "Calls a whois server to retrieve information about a domain name.";
		
	}

	@Override
	public boolean isDeprecated(String arg0) {
		
		return false;
		
	}
	
	public String js_query(String domainName, String server, int port, int timeout) {
			
		try {
			
			Socket socket = new Socket(server, port);
			socket.setSoTimeout(timeout);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			out.writeBytes(domainName + "\r\n");
			
			String strl = null;
			StringBuffer buffer = new StringBuffer();
			while ((strl = in.readLine()) != null)
			{
				buffer.append(strl);
				buffer.append("\r\n");
			}
			
			out.close();
			in.close();
			socket.close();
			
			return buffer.toString();
			
		} catch (IOException ioEx) {
			
			return ioEx.getLocalizedMessage();
			
		} catch (Exception ex) {
			
			return ex.getLocalizedMessage();
			
		}
		
	}

	/**
	 * @return the server
	 */
	public String js_getServer() {
		
		return server;
		
	}

	/**
	 * @param server the server to set
	 */
	public void js_setServer(String server) {
		
		this.server = server;
		
	}

	/**
	 * @return the port
	 */
	public int js_getPort() {
		
		return port;
		
	}

	/**
	 * @param port the port to set
	 */
	public void js_setPort(int port) {
		
		this.port = port;
		
	}

	/**
	 * @return the timeout
	 */
	public int js_getTimeout() {
		
		return timeout;
		
	}

	/**
	 * @param timeout the timeout to set
	 */
	public void js_setTimeout(int timeout) {
		
		this.timeout = timeout;
		
	}
	
	public String js_query(String domainName) {
		
		return js_query(domainName, this.server, this.port, this.timeout);
		
	}
	
	public String js_query(String domainName, String server) {
		
		return js_query(domainName, server, this.port, this.timeout);
		
	}
	
	public String js_query(String domainName, String server, int port) {
		
		return js_query(domainName, server, port, this.timeout);
		
	}

}
