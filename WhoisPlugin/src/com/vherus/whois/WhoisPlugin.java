/**
 * 
 */
package com.vherus.whois;

import java.beans.PropertyChangeEvent;
import java.net.URL;
import java.util.Properties;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.servoy.j2db.plugins.IClientPlugin;
import com.servoy.j2db.plugins.IClientPluginAccess;
import com.servoy.j2db.plugins.PluginException;
import com.servoy.j2db.scripting.IScriptable;

/**
 * @author Nathan King <nkvherus@gmail.com>
 *
 */
public class WhoisPlugin implements IClientPlugin {
	
	private WhoisPluginProvider provider;

	@Override
	public Properties getProperties() {
		
		Properties props = new Properties();
		props.put(DISPLAY_NAME, getName());
		return props;
		
	}

	@Override
	public void load() throws PluginException {

	}

	@Override
	public void unload() throws PluginException {
		
		provider = null;

	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {

	}

	@Override
	public IScriptable getScriptObject() {
		
		if (provider == null)
		{
			provider = new WhoisPluginProvider();
		}
		return provider;
		
	}

	@Override
	public Icon getImage() {
		
		URL iconUrl = getClass().getResource("images/whois.png");
		
		if (iconUrl != null)
		{
			return new ImageIcon(iconUrl);
		}
		else
		{
			return null;
		}
		
	}

	@Override
	public String getName() {

		return "whois";
		
	}

	@Override
	public void initialize(IClientPluginAccess arg0) throws PluginException {

	}

}
